var express = require('express');
var path = require('path');
var app = express();

app.use('/', express.static('dist/AngularRealtime'));

var PORT = 3500;
app.get('/',function(req,res){
  res.sendFile(path.join(__dirname, 'dist/AngularRealtime/index.html'));
});
app.listen(PORT, function() {
 console.log('server running on http://localhost:' + PORT);
});
