import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import { NgForm,FormGroup, FormArray, FormBuilder, Validators,ReactiveFormsModule } from '@angular/forms';

import io from 'socket.io-client';
const socket = io('http://5.189.137.34:8080');

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})

export class ProductsComponent implements OnInit {

  public newProduct = {};
  public products   = [];
  public formAddProduct: FormGroup;

  constructor( private changeDetection: ChangeDetectorRef, private fb: FormBuilder  ) { }

  ngOnInit() {

    //Formulario para agragar producto
    this.formAddProduct = this.fb.group({
      name: ['', Validators.compose([ Validators.required]) ],
      company: ['', Validators.compose([ Validators.required])],
      price: ['', Validators.compose([ Validators.required])]
    });

    //Obtiene listado de productos
    socket.on('getProducts',(data)=> {
      this.products = data;
      this.changeDetection.detectChanges();
   });

   //Escucha cuando un producto es agradado
   socket.on('addProduct',(data)=> {
     this.products.push(data);
     this.changeDetection.detectChanges();
  });

  }

  addProduct( form ) {

    if ( form.valid ) {

      let product     = form.value;
      product['id']   = this.products.length + 1;

      this.products.push(product);
      socket.emit('newProduct', product)

      this.formAddProduct.reset();
    }

  }

}
